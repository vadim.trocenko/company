const company = new Company('Yojji', 5);
const admin = Company.createSuperAdmin(company, 'admin', 'admin');


addHeader(company);

company.registerUserCreateCallback(addUser);
company.registerUserCreateCallback(() => {
  addNotification(company.name, 'Add new user');
});

company.registerUserUpdateCallback(updateDeleteUser);
company.registerUserUpdateCallback(function(user) {
  if (!user.id) {
    addNotification(company.name, 'deleted user!');
    return;
  };
  addNotification(company.name, 'update user!');
});