const headerTemplate = document.querySelector('#header');
const userTemplate = document.querySelector("#user");
let tbody;

const addHeader = ({name, maxSize}) => {
  const header = headerTemplate.content.cloneNode(true);
  const companyName = header.querySelector('#companyName');
  const maxSizeCounter = header.querySelector('#maxSize');

  
  companyName.textContent = name;
  maxSizeCounter.textContent = maxSize;
  document.body.append(header);
  tbody = document.querySelector('#tbody');
};

const addUser = (user, curSize) => {
  const {name, lastName, isAdmin, role, id} = user;
  const curSizeUsers = document.querySelector('#curSize');
  curSizeUsers.textContent = curSize;
  const clone = userTemplate.content.cloneNode(true);
  let td = clone.querySelectorAll('td');
  td[0].textContent = '*';
  td[1].textContent = name;
  td[2].textContent = lastName;
  td[3].textContent = isAdmin;
  td[4].textContent = role;
  td[5].textContent = id;

const delButton = clone.querySelector('#close');
  delButton.addEventListener('click', event => {
    admin.deleteUser(id);
    const curSizeUsers = document.querySelector('#curSize');
    curSizeUsers.textContent = curSize - 1;
    event.target.parentElement.parentElement.remove();
    addNotification('Company',`delete user ${name}`);
  });
  tbody.append(clone);
};

const addNotification = (name, message) => {
  const notificationTemplate = document.querySelector('#notification');
  const clone = notificationTemplate.content.firstElementChild.cloneNode(true);
  const notificationDescription = clone.querySelector('.toast-description');


  notificationDescription.textContent = `${name} ${message}`;
  document.body.append(clone);
  setTimeout(() => {clone.remove()}, 2000);
};

const updateDeleteUser = (userData, companyCurSize) => {
  let usersId = document.querySelectorAll('#user-id');
  if (!userData.id) {
    for (let userId of usersId) {
      if (Number(userId.textContent) === userData) {
        let userParent = userId.parentNode;
        userParent.remove();
        const curSize = document.querySelector('#curSize');
        curSize.textContent = companyCurSize;
      }
    }
    return
  }
  
  for (let userId of usersId) {
    if (Number(userId.textContent) === userData.id) {
      let userParent = userId.parentNode;
      userParent.children[1].textContent = userData.name;
      userParent.children[2].textContent = userData.lastName;
      userParent.children[3].textContent = userData.isAdmin;
      userParent.children[4].textContent = userData.role;
      userParent.children[5].textContent = userData.id;
    };
  };
};
