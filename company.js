
;(function() {
  const TOKEN = 'secret token';
  const ERROR_MESSAGES = {
    ADMIN_ERROR: 'Admin already has',
    ADMIN_DEL_ERROR: 'Can`t delete Admin!',
    LIMIT_ERROR: 'Maximum limit exceeded!',
    PASSWORD_MASSAGE: 'Enter password',
    PASSWORD_ERROR: 'Wrong password',
    TOKEN_ERROR: 'Wrong token!!!',
  };

  const usersContainer = new WeakMap();
  const callbackCollections = new WeakMap();


  class Company {
    curSize = 0;
    #adminCheck = false;

    static createSuperAdmin (company, name, lastName) {

      if (company.#adminCheck) {
        throw new Error(ERROR_MESSAGES.ADMIN_ERROR);
      };

      let users = [];
      const admin = new Admin(name, lastName);


      admin.company = company;
      users.push(admin);
      usersContainer.set(company, users);
      company.curSize++
      company.#adminCheck = true;
      return admin;
    };


    getUser(id) {
      return usersContainer.get(this).find(item => item.id === id);
    };

    registerUserCreateCallback(callback) {
      callbackCollections.get(this).registerUserCreateCallback.push(callback);
    };

    registerUserUpdateCallback(callback) {
      callbackCollections.get(this).registerUserUpdateCallback.push(callback);
    };

    registerNoSpaceNotifyer(callback) {
      callbackCollections.get(this).registerNoSpaceNotifyer.push(callback);
    };

    constructor(name, maxSize) {
      this.name = name;
      this.maxSize = maxSize
      let callbackCollection = {
        'registerUserCreateCallback' : [],
        'registerUserUpdateCallback' : [],
        'registerNoSpaceNotifyer' : [],
      };
      callbackCollections.set(this, callbackCollection);
    };
  };

  class User {
    constructor (name, lastName, isAdmin, role) {
      this.id = Math.floor(Math.random() * 100);
      this.name = name;
      this.lastName = lastName;
      this.isAdmin = isAdmin;
      this.role = role;
    };

  };

  class Admin extends User {
    #token;
    #password;
    createUser(name, lastName, isAdmin, role) {

      if (this.#token != TOKEN) {
        throw new Error(ERROR_MESSAGES.TOKEN_ERROR);
      };

      if (this.company.curSize >= this.company.maxSize) {
        throw new Error(ERROR_MESSAGES.LIMIT_ERROR);
      };

      if (prompt(ERROR_MESSAGES.PASSWORD_MASSAGE) !== this.#password) {
        throw new Error(ERROR_MESSAGES.PASSWORD_ERROR);
      };

      const users = usersContainer.get(this.company);
      const user = new User(name, lastName, isAdmin, role);
      const self = this;


      const proxedUser = new Proxy(user, {
        set(target, property, value) {
          target[property] = value;
          callbackCollections.get(self.company).registerUserUpdateCallback.forEach((callBack) => {
                callBack(target, self.company.curSize);
          });
          return true;
        }
      });

      const {callbackCollection, curSize} = this.company;
      
      user.callbackCollection = callbackCollection;
      user.curSize = curSize;
      users.push(proxedUser);
      this.company.curSize++;

      callbackCollections.get(self.company).registerUserCreateCallback.forEach((callBack) => {
        callBack(user, this.company.curSize);
      });

      return proxedUser;
    };

    deleteUser(id) {

      if (prompt(ERROR_MESSAGES.PASSWORD_MASSAGE) !== this.#password) {
        throw new Error(ERROR_MESSAGES.PASSWORD_ERROR);
      };

      if (this.#token != TOKEN) {
        throw new Error(ERROR_MESSAGES.TOKEN_ERROR)
      };

      const users = usersContainer.get(this.company);

      usersContainer.set(this.company, users.filter(item => {

        if (id === users[0].id) {
          throw new Error(ERROR_MESSAGES.ADMIN_DEL_ERROR);
        };

        item.id !== id
      }));
      this.company.curSize--;
      callbackCollections.get(this.company).registerUserUpdateCallback.forEach((callBack) => {
        callBack(id, this.company.curSize);
      });
    };

    constructor(name, lastName, isAdmin, role) {
      super(name, lastName, isAdmin, role);
      this.#token = TOKEN;
      this.#password = prompt(ERROR_MESSAGES.PASSWORD_MASSAGE);
      this.role = role;
    };
  };

  window.Company = Company;

})();